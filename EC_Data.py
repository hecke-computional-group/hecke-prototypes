class EC_Data:
    """
    A class to storing, loading, and querying eliptic curve isogeny class data.
    """

    def __init__( self ):
        """ Just creates the dictionary that will hold the data. """
        self.data = dict()

    def load( self, file: str ) -> None:
        """ Takes a filename that contains the EC data, parses it, and loads it into the dictionary. """
        with open( file, "r" ) as f:
            for line in f.read().strip().split("\n"):
                line = line.split(",")
                self.data[int(line[0].strip())] = int( line[1].strip() )

    def get( self, val: int ) -> int:
        """ Takes a conductor value, and returns the data for that conductor (0 if no data exists). """
        if val in self.data:
            return self.data[val]
        else:
            return 0

