# This is a rough draft of the script base on the meeting on 7/27/2021

from sage.all_cmdline import *   # import sage library

from EC_Data import EC_Data


_sage_const_1 = Integer(1)

def getIntInput( message: str ) -> int:
    """ Takes input from the user until they give an int as input and returns that int."""
    while True:
        try:
            tmp = input( f'{message} (as an integer): ' )
            N = int( tmp )
            return N
        except ValueError:
            print( f'{tmp!r} is not an integer.' )

def compute( i: int ):
    try:
        print( f'{i}: {db.ec_curvedata.count_distinct("lmfdb_iso", {"conductor": i})}' )
    except:
        print( f'Ran into a problem. Passing on {i}.' )


def main( ) -> None:
    """ The main function of this script. """
    # N: int = getIntInput( message="Please input a level:" )
    # k: int = getIntInput( message="Please input a weight:" )
    # M = ModularForms(k,N).cuspidal_submodule().modular_symbols( sign=_sage_const_1  )
    data = EC_Data()
    data.load( "ec_data.csv" )
    while True:
        cond = getIntInput( "Input a conductor: " )
        d = data.get( cond )
        if d != 0:
            print( f'Found, {cond}: {d}' )
        else:
            print( f'{cond} is not a valid conductor.' )


if __name__ == "__main__":
    main()
