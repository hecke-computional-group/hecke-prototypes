def FibSeries( n ):
    if n == 1:
        return n
    return n * FibSeries( n - 1 )
