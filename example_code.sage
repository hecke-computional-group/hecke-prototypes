M = ModularForms(1,100).cuspidal_submodule().modular_symbols(sign=1)
f2 = M.hecke_operator(2).charpoly('x')
K2.<a2> = NumberField(f2)
K2.ring_of_integers().basis()
